import paymentutil as pu

#Class to represent a transaction/payment
class Payment(object):

    def __init__(self, actor, target, tstamp):
        self.__actor = actor
        self.__target = target
        self.__tstamp = tstamp

    @property
    def actor(self):
        return self.__actor

    @property
    def target(self):
        return self.__target

    @property
    def tstamp(self):
        return self.__tstamp

    @classmethod
    def fromJsonLine(cls, line):
        #Loads a transaction directly from json.
        return cls(line["actor"], line["target"], pu.parseCreatedTime(line["created_time"]))

    def printPayment(self):
        #Outputs transaction to console.
        print "actor: " + self.actor + "; target: " + self.target + "; tstamp: " + str(self.tstamp)

    def validatePayment(self):
        #Performs basic checks on transaction to mae sure it's valid.
        if(self.actor!='' and self.target!='' and pu.isValidTimestamp(self.tstamp)):
            return True
        else:
            return False

    def __str__(self):
        return "payment from " + self.actor + " to " + self.target + " at " + str(self.tstamp)

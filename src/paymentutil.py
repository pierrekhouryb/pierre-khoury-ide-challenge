import datetime, dateutil.parser
#Methods to process transaction timestamps
def parseCreatedTime(created_time):
	#parse created_time as datetime object based on ISO 8601
	try:
		return dateutil.parser.parse(created_time)
	except:
		return -1

def isValidTimestamp(tstamp):
    return isinstance(tstamp, datetime.datetime)

def initializeTimestamp():
    #assuming here no transaction could have been done before venmo release date
    #nice to have a relatively recent time origin, but it might preventthe use
    #of this code through other paypal products
    return parseCreatedTime('2009-01-01T00:00:00Z')

def setTimestampDeltaInSeconds(delta):
    return datetime.timedelta(seconds=delta)

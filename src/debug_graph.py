from graph import Graph
from payment import Payment
import mathutil as mu

#Sample with actual data
g = Graph()

print "PAYMENT"
p = Payment.fromJsonLine({u'created_time': u'2016-04-07T03:34:18Z', u'target': u'Cary-Gitter', u'actor': u'Megan-Braverman'})
p.printPayment()
print "Is transaction recent enough? " + str(g.isTransactionRecentEnough(p.tstamp))
print "Is transaction too old? " + str(g.isTransactionTooOld(p.tstamp))
g.addTransaction(p.actor, p.target, p.tstamp)
print "max timestamp is: " + str(g.getMaxTimestamp())
degrees = g.getDegreesList()
print "Degrees median of: " + str(degrees) + " is " + str(mu.computeMedianDegree(degrees))

print "PAYMENT"
p = Payment.fromJsonLine({u'created_time': u'2016-04-07T03:34:58Z', u'target': u'Louis-Thomas-Hardin', u'actor': u'Megan-Braverman'})
p.printPayment()
print "Is transaction recent enough? " + str(g.isTransactionRecentEnough(p.tstamp))
print "Is transaction too old? " + str(g.isTransactionTooOld(p.tstamp))
g.addTransaction(p.actor, p.target, p.tstamp)
print "max timestamp is: " + str(g.getMaxTimestamp())
degrees = g.getDegreesList()
print "Degrees median of: " + str(degrees) + " is " + str(mu.computeMedianDegree(degrees))

print "PAYMENT"
p = Payment.fromJsonLine({u'created_time': u'2016-04-07T03:34:00Z', u'target': u'Louis-Thomas-Hardin', u'actor': u'Megan-Braverman'})
p.printPayment()
print "Is transaction recent enough? " + str(g.isTransactionRecentEnough(p.tstamp))
print "Is transaction too old? " + str(g.isTransactionTooOld(p.tstamp))
g.addTransaction(p.actor, p.target, p.tstamp)
print "max timestamp is: " + str(g.getMaxTimestamp())
degrees = g.getDegreesList()
print "Degrees median of: " + str(degrees) + " is " + str(mu.computeMedianDegree(degrees))

print "PAYMENT"
p = Payment.fromJsonLine({u'created_time': u'2016-04-07T03:31:18Z', u'target': u'Louis-Thomas-Hardin', u'actor': u'Megan-Braverman'})
p.printPayment()
print "Is transaction recent enough? " + str(g.isTransactionRecentEnough(p.tstamp))
print "Is transaction too old? " + str(g.isTransactionTooOld(p.tstamp))
g.addTransaction(p.actor, p.target, p.tstamp)
print "max timestamp is: " + str(g.getMaxTimestamp())
degrees = g.getDegreesList()
print "Degrees median of: " + str(degrees) + " is " + str(mu.computeMedianDegree(degrees))

print "PAYMENT"
p = Payment.fromJsonLine({u'created_time': u'2016-04-07T03:35:02Z', u'target': u'Louis-Thomas-Hardin', u'actor': u'Jacques-Prevert'})
p.printPayment()
print "Is transaction recent enough? " + str(g.isTransactionRecentEnough(p.tstamp))
print "Is transaction too old? " + str(g.isTransactionTooOld(p.tstamp))
g.addTransaction(p.actor, p.target, p.tstamp)
print "max timestamp is: " + str(g.getMaxTimestamp())
degrees = g.getDegreesList()
print "Degrees median of: " + str(degrees) + " is " + str(mu.computeMedianDegree(degrees))

print "PAYMENT"
p = Payment.fromJsonLine({u'created_time': u'2016-04-07T03:37:30Z', u'target': u'Louis-Thomas-Hardin', u'actor': u'Jacques-Prevert'})
p.printPayment()
print "Is transaction recent enough? " + str(g.isTransactionRecentEnough(p.tstamp))
print "Is transaction too old? " + str(g.isTransactionTooOld(p.tstamp))
g.addTransaction(p.actor, p.target, p.tstamp)
print "max timestamp is: " + str(g.getMaxTimestamp())
degrees = g.getDegreesList()
print "Degrees median of: " + str(degrees) + " is " + str(mu.computeMedianDegree(degrees))

print "PAYMENT"
p = Payment.fromJsonLine({u'created_time': u'2016-04-07T03:36:31Z', u'target': u'Louis-Thomas-Hardin', u'actor': u'Jacques-Prevert'})
p.printPayment()
print "Is transaction recent enough? " + str(g.isTransactionRecentEnough(p.tstamp))
print "Is transaction too old? " + str(g.isTransactionTooOld(p.tstamp))
g.addTransaction(p.actor, p.target, p.tstamp)
print "max timestamp is: " + str(g.getMaxTimestamp())
degrees = g.getDegreesList()
print "Degrees median of: " + str(degrees) + " is " + str(mu.computeMedianDegree(degrees))

print "PAYMENT"
p = Payment.fromJsonLine({u'created_time': u'2016-04-07T03:36:30Z', u'target': u'Cary-Gitter', u'actor': u'Jacques-Prevert'})
p.printPayment()
print "Is transaction recent enough? " + str(g.isTransactionRecentEnough(p.tstamp))
print "Is transaction too old? " + str(g.isTransactionTooOld(p.tstamp))
g.addTransaction(p.actor, p.target, p.tstamp)
print "max timestamp is: " + str(g.getMaxTimestamp())
degrees = g.getDegreesList()
print "Degrees median of: " + str(degrees) + " is " + str(mu.computeMedianDegree(degrees))

#Check
print "+++ Graph of transactions:"
for v in g:
    print "#Degree for " + v.getId() + " = " + str(v.getDegree())
    for w in v.getConnections():
        print("( %s , %s, %s )" % (v.getId(), w.getId(), v.getWeight(w)))

from payment import Payment
import paymentutil as pu


#Try to initialize a timestamp
t0 = pu.initializeTimestamp()
print str(t0) + " is valid? " + str(pu.isValidTimestamp(t0))




#Try to make payments
print "+++ good payment"
p = Payment('pierre_khoury','pauline_durand',pu.parseCreatedTime('2016-02-28T23:23:12Z'))
p.printPayment()
print "actor: " + p.actor + "; target: " + p.target + "; tstamp: " + str(p.tstamp)
print p.validatePayment()

print "+++ bad payment: the timestamp should be a datetime object"
p = Payment('pierre_khoury','pauline_durand','2016-02-28T23:23:12Z')
p.printPayment()
print p.validatePayment()

print "+++ bad payment: actor is empty string"
p = Payment('','pauline_durand',pu.parseCreatedTime('2016-02-28T23:23:12Z'))
p.printPayment()
print p.validatePayment()

print "+++ payment from json line"
p = Payment.fromJsonLine({u'created_time': u'2016-03-28T23:23:12Z', u'target': u'Cary-Gitter', u'actor': u'Megan-Braverman'})
p.printPayment()
print p.validatePayment()

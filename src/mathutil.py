import math

#Method to compute median degree of a sorted list
def computeMedianDegree(sorteddegrees):
    #median is 0 if the list is empty
    if not sorteddegrees:
        return 0

    middle = len(sorteddegrees) / 2
    if (len(sorteddegrees) % 2) == 0:
        #even: compute mean of 2 middle numbers
        return (sorteddegrees[middle-1] + sorteddegrees[middle])/2.0
    else:
        #odd: return middle number
        return float(sorteddegrees[middle])

def truncateMedianAsString(f, n):
    #from http://stackoverflow.com/questions/783897/truncating-floats-in-python?noredirect=1&lq=1
    s = '{}'.format(f)
    i, p, d = s.partition('.')
    return '.'.join([i, (d+'0'*n)[:n]])

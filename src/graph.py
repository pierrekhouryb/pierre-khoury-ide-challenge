import paymentutil as pu

class Vertex:
    #This class represents a vertex. A vertex has an id,
    #a dictionary of connected vertices and their edge's weight,
    #and a degree.
    def __init__(self,key):
        self.id = key
        self.connectedTo = {}

    def addNeighbor(self,nbr,weight=0):
        self.connectedTo[nbr] = weight

    def removeNeighbor(self,nbr):
        del self.connectedTo[nbr]

    def __str__(self):
        return str(self.id) + ' connectedTo: ' + str([x.id for x in self.connectedTo])

    def getConnections(self):
        return self.connectedTo.keys()

    def getId(self):
        return self.id

    def getWeight(self,nbr):
        return self.connectedTo[nbr]

    def getDegree(self):
        return len(self.connectedTo)

class Graph:
    #This class represents the transaction graph. The graph has a dictionary of
    #its vertices (and edges), a maximum timestamp, and a time-window size.
    def __init__(self):
        self.vertList = {}
        #Timestamp of the most recent transaction
        self.maxTimestamp = pu.initializeTimestamp()
        #Size of the time-window in seconds
        self.deltaTimestamp = pu.setTimestampDeltaInSeconds(60);

    def addVertex(self,key):
        newVertex = Vertex(key)
        self.vertList[key] = newVertex
        return newVertex

    def getVertex(self,n):
        if n in self.vertList:
            return self.vertList[n]
        else:
            return None

    def __contains__(self,n):
        return n in self.vertList

    def addEdge(self,f,t,cost=0):
        if f not in self.vertList:
            nv = self.addVertex(f)
        if t not in self.vertList:
            nv = self.addVertex(t)
        self.vertList[f].addNeighbor(self.vertList[t], cost)

    def getVertices(self):
        return self.vertList.keys()

    def __iter__(self):
        return iter(self.vertList.values())

    #Methods specific to a Venmo transaction graph.
    #Could be refactored in derived class
    def getMaxTimestamp(self):
        return self.maxTimestamp

    def setMaxTimestamp(self, mt):
        if self.maxTimestamp < mt:
            self.maxTimestamp = mt
            self.refreshTransactions()

    def addTransaction(self,actor,target,tstamp):
        flag = 0 #flag to check if the transaction is new
        #add actor as a vertex if it doesn't exist
        if actor not in self.vertList:
            self.addVertex(actor)
            flag = 1
        #add target as a vertex if it doesn't exist
        if target not in self.vertList:
            self.addVertex(target)
            flag = 1

        #even if vertices already exist, check if they are connected
        if target not in self.vertList[actor].getConnections():
            flag = 1

        #add timestamp as an edge if transaction is new,
        if flag == 1:
            self.addEdge(actor, target, tstamp)
            self.addEdge(target, actor, tstamp)
            self.setMaxTimestamp(tstamp)
        else:#or if it is more recent, update the existing edge
            if self.vertList[actor].getWeight(self.vertList[target]) < tstamp:
                self.addEdge(actor, target, tstamp)
                self.addEdge(target, actor, tstamp)
                self.setMaxTimestamp(tstamp)

    def refreshTransactions(self):
        #Remove transactions that are older than delta seconds
        transactionsToRemove = list()
        #For all vertex in the graph
        for v in self.vertList:
            #and for all of its transactions
            for w in self.vertList[v].getConnections():
                #verify that the transaction date is still in the time-window
                if self.isTransactionTooOld(self.vertList[v].getWeight(w)):
                    #if not, add it to the list to be removed
                    curr_t = sorted((self.vertList[v].getId(), w.getId()));
                    #unless it is already there. To-do - improvement: move this
                    #earlier to avoid checking the same transaction twice.
                    if curr_t not in transactionsToRemove:
                        transactionsToRemove.append(curr_t)

        #Go through all transactions to remove and effectively delete them from
        #the transaction graph.
        for t in transactionsToRemove:
            self.removeTransaction(t[0], t[1])


    def removeTransaction(self, actor, target):
        #Remove the connection between the two vertices
        self.vertList[actor].removeNeighbor(self.vertList[target])
        self.vertList[target].removeNeighbor(self.vertList[actor])
        #and remove the vertex if it's not connected to the graph anymore.
        if len(self.vertList[actor].getConnections()) == 0:
            del self.vertList[actor]
        if len(self.vertList[target].getConnections()) == 0:
            del self.vertList[target]

    def isTransactionTooOld(self, tstamp):
        #Compute the age of the transaction relative to the maximum timestamp.
        diff = self.maxTimestamp - tstamp
        #Check if it is too old.
        if(diff > self.deltaTimestamp):
            return True #too old
        else:
            return False #not too old

    def isTransactionRecentEnough(self, tstamp):
        #Compute the lower bound of the time-window based on the maximum
        #timestamp
        lowerBound = self.maxTimestamp - self.deltaTimestamp
        #Check if the transaction date is in the time-window.
        if(tstamp > lowerBound):
            return True #within time window
        else:
            return False #outside of window

    def getDegreesList(self):
        degrees = []
        #Create a list of all degrees in the graph
        for v in self.vertList:
            degrees.append(self.vertList[v].getDegree())
        #To-do - improvement: optimize with sorted insert in list creation
        degrees.sort()
        return degrees

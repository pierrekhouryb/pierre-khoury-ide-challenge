#Script to debug rolling_median.

import sys
import json
from payment import Payment
from graph import Graph
import mathutil as mu
import paymentutil as pu

#Make sure we have the right inputs
if len(sys.argv) != 3:
	print "Usages are: python main.py arg1 arg2"
	sys.exit()

#Start
print "Running " + sys.argv[0]
fin = sys.argv[1]
fout = sys.argv[2]

print "input: " + fin
print "output: " + fout

#Block to stream
with open(fin) as data_file, open(fout, 'w+') as output_file:
	g = Graph()
	counter = 0
	for transaction in data_file:
		try:
			json_data = json.loads(transaction)
			counter = counter + 1
		except:
			#handle streaming errors here
			print("Streaming error after transaction #%d " % counter)
			json_data = 0
			pass
		if json_data:
			#parse data for current payment
			p = Payment.fromJsonLine(json_data)
			print("Transaction #%d :" % counter)
			p.printPayment()
			#p.printPayment()
			if p.validatePayment():
				#verify current payment is valid
				if g.isTransactionRecentEnough(p.tstamp):
					#process payment
					g.addTransaction(p.actor, p.target, p.tstamp)
					#compute and output median
					median = mu.computeMedianDegree(g.getDegreesList())
					#and truncate to 2 decimals
					median = mu.truncateMedianAsString(median,2)
					print("%s\n" % str(median))
				#output rolling median
				output_file.write("%s\n" % str(median))

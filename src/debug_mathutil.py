import mathutil as mu
import testutil as tu

#
degrees = []
exp_median = 0
comp_median = mu.computeMedianDegree(degrees)
tu.assertThatEqual(exp_median, comp_median)
#
degrees = [1,1]
exp_median = 1
comp_median = mu.computeMedianDegree(degrees)
tu.assertThatEqual(exp_median, comp_median)
#
degrees = [1,1,1,1]
exp_median = 1
comp_median = mu.computeMedianDegree(degrees)
tu.assertThatEqual(exp_median, comp_median)
#
degrees = [1,1,2]
exp_median = 1
comp_median = mu.computeMedianDegree(degrees)
tu.assertThatEqual(exp_median, comp_median)
#
degrees = [1,1,2,2]
exp_median = 1.5
comp_median = mu.computeMedianDegree(degrees)
tu.assertThatEqual(exp_median, comp_median)
#
degrees = [1,2,2,3]
exp_median = 2
comp_median = mu.computeMedianDegree(degrees)
tu.assertThatEqual(exp_median, comp_median)
#
degrees = [1,2,2,3,3]
exp_median = 2
comp_median = mu.computeMedianDegree(degrees)
tu.assertThatEqual(exp_median, comp_median)
#
val = 1.66666667
exp_val = 1.66
trunc_val = mu.truncateMedianAsString(val, 2)
tu.assertThatEqual(exp_val, trunc_val)

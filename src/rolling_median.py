#rolling_median.py
#tool to anayze Venmo's dataset.

import sys
import json
from payment import Payment
from graph import Graph
import mathutil as mu
import paymentutil as pu

#Make sure we have the right inputs
if len(sys.argv) != 3:
	print('Usages are: python rolling_median.py arg1 arg2')
	sys.exit()

#Start
print('Running %s' % sys.argv[0])
fin = sys.argv[1]
fout = sys.argv[2]

print('input: %s' % fin)
print('output: %s' % fout) 

with open(fin) as data_file, open(fout, 'w+') as output_file:
	#create the Venmo transaction graph
	g = Graph()
	#start streaming the transactions
	for transaction in data_file:
		try:
			#receive a transaction
			json_data = json.loads(transaction)
		except:
			#handle streaming errors here
			json_data = 0
			pass
		if json_data:
			#parse data for current payment
			p = Payment.fromJsonLine(json_data)
			if p.validatePayment():
				#verify current payment is valid
				if g.isTransactionRecentEnough(p.tstamp):
					#process payment
					g.addTransaction(p.actor, p.target, p.tstamp)
					#compute median
					median = mu.computeMedianDegree(g.getDegreesList())
					#and truncate to 2 decimals
					median = mu.truncateMedianAsString(median,2)
				#output rolling median
				output_file.write("%s\n" % str(median))
